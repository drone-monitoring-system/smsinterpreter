import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
from twilio.rest import Client
from flask import Flask, request
from twilio import twiml
from twilio.twiml.messaging_response import MessagingResponse

account_sid = "AC6710ae8414417204fe9e0ddf03c51950"
auth_token = "0ceaf39d0b1ed11d0e6d7c0871c8540f"
client = Client(account_sid, auth_token)

# Fetch the service account key JSON file contents
cred = credentials.Certificate('drone-monitoring-system-firebase-adminsdk.json')

# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://drone-monitoring-system.firebaseio.com'
})

# Code to send a message
# client.messages.create(
#     to="<INPUT PHONE NUMBER TO SEND SMS TO>",
#     from_="+15612216812",
#     body="This is a test sending from python"
# )

# Starting SMSInterpreter Instructions
# 1. Start Python Script
# 2. Write "ngrok http 5000" in console
# 3. Change "A MESSAGE COMES IN" URL with forwarding address from ngrok console

app = Flask(__name__)


@app.route("/", methods=['POST'])
def sms_reply():
    number = request.form['From']
    message_body = request.form['Body']

    if "," in message_body and len(message_body.split(',')) == 4:
        SMSValues = message_body.split(',')

        # CODE TO MAKE FIREBASE DB CHECKS
        # As an admin, the app has access to read and write all data, regradless of Security Rules
        ref = db.reference('Drone_data')

        snapshot = ref.order_by_key().get()

        oldID = ""

        print('\'id\':\'' + SMSValues[0] + '\'')

        for key, value in snapshot.items():
            strVal = format(value)
            print(key, value)
            if strVal.__contains__('\'id\': \'' + SMSValues[0] + '\''):
                oldID = key
                break

        if oldID == "":
            print("new value encountered!")
            # push in new value
            pushRef = ref.push()
            pushRef.set({
                'id': SMSValues[0],
                'heading_angle': SMSValues[1],
                'latitude': SMSValues[2],
                'longitude': SMSValues[3]
            }
            )
        else:
            print("old value encountered!")
            # update existing value
            updateRootRef = db.reference()
            updateRef = updateRootRef.child("Drone_data")
            updateRef.update({
                oldID: {
                    'id': SMSValues[0],
                    'heading_angle': SMSValues[1],
                    'latitude': SMSValues[2],
                    'longitude': SMSValues[3]
                }
            })

    resp = MessagingResponse()
    resp.message('MESSAGE RECEIVED!')
    return str(resp)


if __name__ == '__main__':
    app.run(debug=True)
